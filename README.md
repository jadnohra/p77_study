# Math notation on gitlab

[Manual](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md#math)

$`\KaTeX`$

This math is inline $`a^2+b^2=c^2`$.

This is on a separate line
```math
a^2+b^2=c^2
```

More symbols: $` \int_{-\infty} \alpha \partial `$